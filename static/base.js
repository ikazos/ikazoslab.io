$(document).ready(function() {
    $("#home-image").mouseenter(function() {
        $(this).transition("pulse");
    })

    $("#home-headers").hover(
        function() {
            $(this).css("text-decoration", "underline");
        },
        function() {
            $(this).css("text-decoration", "");
        }
    );
});