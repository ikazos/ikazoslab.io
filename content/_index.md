+++
title = "Satoru Ozaki (ikazos)"
date = 2020-08-02
+++

I am a first-year graduate student in the Master of Language Technologies program at [Language Technologies Institute](https://lti.cs.cmu.edu/) at [Carnegie Mellon University](https://www.cmu.edu/) in Pittsburgh, USA.

I write about languages, linguistics and NLP on this website.

As you can see, this website is still under heavy construction. It will be updated very often.