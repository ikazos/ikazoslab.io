+++
title = "About me"
date = 2020-08-02
insert_anchor_links = "right"
+++

I am a first-year graduate student in the Master of Language Technologies program at [Language Technologies Institute](https://lti.cs.cmu.edu/) at [Carnegie Mellon University](https://www.cmu.edu/) in Pittsburgh, USA. I also received my B. A. in Linguistics from CMU, graduating with both University and College Honors. I did my undergraduate thesis on a topic in theoretical linguistics -- syntax, in particular; I argued for a dependent case-theoretic analysis of case assignment in Japanese.

## Contact

Email:

><span>ｓｏz</span><span>ａｋi＠ａ</span>ｎｄr<span></span>ｅｗ．<span>cmｕ</span><span>．ｅｄｕ</span>

<div class="ui divider"></div>

The rest of this page is a list of topics I'm interested in.

## Languages

I am a native speaker of Mandarin Chinese and Japanese. I speak English fluently, and Swedish presumably at the CEFR B2 level (I will have to actually take the test though, to be sure). Although I don't speak them<sub>i</sub>, I am interested in \[languages of the Caucasus and Central Asia\]<sub>i</sub>. Aside from living languages, I have always been interested in old, i.e. extinct languages. Old Icelandic is actually what got me into linguistics.

### Languages I speak

I speak, with different levels of competence:

### English (English)

...with an unidentifiable accent.

### 汉语/漢語 (Mandarin Chinese)

...with a somewhat Southern Chinese accent. My accent is not Northern Chinese as I pronounce \[iŋ\] as \[in\], e.g. in 北京 'Peking' [peɪ̯²¹⁴⁻²¹¹ t͡ɕiŋ⁵⁵], and I don't have r-colored codas, aka. _erhua_. My accent is not exactly Southern Chinese either, because I don't do the same fronting with \[ɤŋ\], which Southerners pronounce as \[ən\], e.g. in 焚书坑儒/焚書坑儒, and I don't front the retroflex consonants either.

Obviously, there are other accents of Mandarin Chinese, which cannot be accounted for by a simple Northern/Southern Chinese distinction.

### 日本語 (Japanese)

...with some interesting choice of accents for certain words (here by _accent_ I'm refering to the phonological concept of accents).

### Svenska (Swedish)

...with a weird Japanese accent.

### Cool languages

...for my personal definition of "coolness".

#### Northeast Caucasian languages (aka. Nakh(o)-Daghestanian languages)

I really want to learn Lezgian, Chechen and/or Ingush one day.

#### "Altaic", i.e. Turkic, Tungusic and/or Mongolic languages

As an undergrad, I took a class in Turkic linguistics, which covered a few of the so-called "Altaic" languages, including Azerbaijani, Evenk, Evenki, Sakha and Turkish. The papers I read in that class have largely shaped my current interests in syntax. I have also been interested in Mongolian ever since I visited Inner Mongolia.

### Old languages

Old languages are interesting because 1) you can look at how they developed into their modern descendants, if any; 2) sometimes they share interesting properties and phenomena with modern languages. My favorite example for (2) is relative clause formation in Akkadian. I have also taught these languages as student-taught courses in CMU. Check out the links to the course websites!

#### Akkadian

Earliest attested written language (?), ancestor of Hebrew, Arabic, and others. _Sprachbundmitglied_ with Sumerian. Cuneiforms. It was spoken for a longer period of time (2000 years) than English (1400 years)! [Link to my course website](https://www.andrew.cmu.edu/user/sozaki/98363/).

#### Old Norse/Old Icelandic
        
Ancestor of Scandinavian languages. Beautifully complicated mix of Germanic V2 word order and free word order. [Link to my course website](https://www.andrew.cmu.edu/user/sozaki/98348.html).

#### Old Irish

Ancestor of Irish, Manx and Scottish Gaelic. Orthography was never standardized. Palatalization and lenition can be lexically triggered on all consonants.

*   **Theoretical linguistics**
*   Natural language processing
*   Computational linguistics
*   **Neurolinguistics**: Although I'm skeptical about the idea that the brain is actually carrying out MP-style syntax, i.e. what generative linguists call the "computational system" (quoting my professor Lori Levin's words, "language could be epiphenomenal; the brain might not be carrying out the exact set of operations which we as syntacticians observe in languages"), I am interested in knowing how MP-style syntax can be a high-level abstraction, or a functional interpretation of what the brain is actually doing to produce/interpret language. I need to learn more about it, and the field seems very promising. I'm trying to follow Cedric Boeckx and Elliot Murphy in this area, and I would appreciate other recommendations/pointers/resources.