+++
title = "Case in Japanese"
date = 2020-08-04
description = "Literature review on Japanese case assignment"
draft = true

[extra]
tags = [ "linguistics", "syntax", "Japanese", "case" ]
+++

This post is a literature review on Japanese case assignment. I will focus on the following questions:

*   What are the properties of Japanese case assignment?
*   Are there any case-related phenomena in Japanese worth discussing?
*   How does Case/case theory work? How has the theory changed over time?

I will also attempt to cover the following topics:

*   Are case-marking particles functional categories?
*   The structure of nominals
*   Nominative/Accusative Conversion
*   Dative/Nominative Conversion
*   Nominative/Genitive Conversion
*   Passives

## Japanese case assignment

Japanese, being a strictly head-final language, marks case with postnominal particles. Perhaps this is one argument for treating nominals as projections headed by postnominal particles? That's what Tonoike (2019) assumes. Maybe it's worth checking out Speas' work on/idea of KPs (Kase Phrases).

```
1.  Naomi ga  hon  o   kat-ta.
    Naomi NOM book ACC buy-PAST
    `Naomi bought a book.'
```

Morphologicaly, Japanese has nominative-accusative alignment: subjects of intransitive verbs receive the same case marking _ga_ with subjects of transitive verbs. Objects of transitive verbs, on the other hand, receive _o_.

```
2.  Kenta ga  warat-ta.
    Kenta NOM laugh-PAST
    `Kenta laughed.'
```

Certain accusative languages use phonologically unmarked, i.e. empty forms for nominative case marking. These languages include Sakha. Japanese does not; the nominative case marker _ga_ is as phonologically marked as any other case marker. Like Japanese is Korean.

## References